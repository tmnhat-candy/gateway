package com.candy.gateway.mq.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
@Slf4j
public class Receiver implements Serializable {

  @RabbitListener(queues = "spring-boot")
  public void ackMessage(String msg) {
    log.info("Acknowledge from AMQP ==> {}", msg);
  }

  public void receiveMessage(String msg) {
    log.info("Receive new msg from AMQP ==> {}", msg);
  }

}
