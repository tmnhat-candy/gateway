package com.candy.gateway.mq;

import com.candy.gateway.mq.config.MsgPayload;
import com.candy.gateway.mq.config.RabbitMQConfig;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/")
public class ResourceController {

  @Autowired RabbitTemplate template;

  @PostMapping
  public void sendMsg(@RequestBody MsgPayload payload) {

    template.convertAndSend(
        RabbitMQConfig.TOPIC_EXCHANGE_NAME,
        "foo.bar.baz",
        payload);
  }

  @GetMapping("/test")
  public ResponseEntity<String> getMsg() {
    template.convertAndSend(
            RabbitMQConfig.TOPIC_EXCHANGE_NAME,
            "foo.bar.baz",
            MsgPayload.builder().build());

    return ResponseEntity.ok("success");
  }
}
